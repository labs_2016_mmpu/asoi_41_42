
/** \file 
 \brief Task_03 source code

 \mainpage
 \image html graphics.PNG "Graphics of models"
 \author Ilkovets Karolina
 \date 12.11.2016
 */


#include "stdafx.h"
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

/// @brief Function for the linear analysis of simulating object
vector<double> linear(int Period, double OutputTemperature, double outWarmth)
{
	vector<double> linearResult;
	linearResult.push_back(OutputTemp);

	for (int period = 1; period < Period; period++)
	{
		linearResult.push_back(0.988 * linearResult.back() + 0.232 * outWarmth);
	}

	return linearResult;
}

/// @brief Function for the non-linear analysis of simulating object
vector <double> NonLinear(int Period, double OutputTemp, double outWarmth)
{
	vector<double> nonLinearResult;
	nonLinearResult.push_back(OutputTemp);
	nonLinearResult.push_back(OutputTemp);

	for (int period = 2; period < Period; period++)
	{
		nonLinearResult.push_back(0.9 * nonLinearResult[period - 1] - 0.001 * pow(nonLinearResult[period - 2], 2.0) + outWarmth + sin(outWarmth));
	}

	return nonLinearResult;
}

/// @brief The main function on project
int main()
{
	int Period = 0;
	double outTemp = 0.0, outWarmth = 0.0;
	cout << "Input period: ";
	cin >> Period;
	cout << "Ouput temp: ";
	cin >> outTemperature;
	cout << "Ouput warmth: ";
	cin >> outWarmth;

	vector<double> linearModel = linear(Period, outTemp, outWarmth);
	vector<double> nonlinearModel = NonLinear(Period, outTemp, outWarmth);

	cout << " #" << "\t" << "Linear" << "\t\t" << "Non-linear" << endl;

	for (int i = 0; i < linearModel.size(); i++)
		cout << i + 1 << "\t" << linearModel[i] << "\t\t" << nonlinearModel[i] << endl;
	system("pause");
	return 0;
}