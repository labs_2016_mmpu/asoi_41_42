#pragma once
#include "BaseClass.h"
#include <iostream>
#include <math.h>

using namespace std;

class LinearModal :
	public BaseClass
{
public:
	LinearModal();
	~LinearModal();
	void calculatelinearModel(double y, double w);


private:
	double q0 = 0, q1 = 0, q2 = 0, e = 0, e1 = 0, e2 = 0, du = 0, u1 = 0, u = 0;
};