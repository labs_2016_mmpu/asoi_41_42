///@file
///@author Bogush Danil
///@14.10.2016
#include <iostream>

///@brief Display message "Hello world!"
int main(int argc, char* argv) {
	std::cout << "Hello World!!!" << std::endl;
	return 0;
}