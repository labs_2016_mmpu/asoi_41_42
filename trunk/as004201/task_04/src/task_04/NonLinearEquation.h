#pragma once
#include "TemperatureChange.h"

///@class NonLinearEquation 
///@class  NonLinearEquation inherited from TemperatureChange

class NonLinearEquation:public TemperatureChange
{
public:
	///@brief show method
	void show();
	///@brief fill method
	void fill();
	NonLinearEquation(int N = 10, int U = 10):TemperatureChange(N, U) {};
	~NonLinearEquation(){};
};

