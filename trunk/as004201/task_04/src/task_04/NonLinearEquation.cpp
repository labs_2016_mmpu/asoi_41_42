#include "NonLinearEquation.h"

void NonLinearEquation::fill() {
	///@brief calculating the nonlinear model
	for (int i = 0; i < NofY; i++) {
		this->Y[i] = 0.9*(i > 0 ? this->Y[i - 1] : 0) - 0.001*pow(i > 1 ? this->Y[i - 2] : 0, 2) + *(this->Ut) + sin(*(this->Ut));
		pid(i);
	}
}

void NonLinearEquation::show() {
	std::cout << "Non linear method" << std::endl << std::endl;
	if (Y[0] == -1)
		this->fill();
	for (int i = 0; i < NofY; i++)
		std::cout << "Y[" << i + 1 << "]" << "\t=\t" << this->Y[i] << std::endl;
	std::cout << std::endl;
}