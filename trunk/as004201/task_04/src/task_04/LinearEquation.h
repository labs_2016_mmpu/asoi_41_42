#pragma once
#include "TemperatureChange.h"

///@class LinearEquation 
///@class LinearEquation inherited from TemperatureChange

class LinearEquation:public TemperatureChange
{
public:
	///@brief show method
	void show();
	///@brief fill method
	void fill();
	LinearEquation(int N = 10, int U = 10) :TemperatureChange(N, U) {};
	~LinearEquation(){};
};

