/** \file TemperatureSystem.h
*  \author Vladislav Kondratuk  (as004113)
*  \brief Declaration of the TemperatureSystem class.
*/
#pragma once

///@brief Abstract class TemperatureSystem
class TemperatureSystem
{
protected:
	///@brief Declare variables to store input/output values.
	double systemInputValue;
	double systemOutputValue;
public:
	///@brief Declare constructor with paramethers.
	TemperatureSystem(double, double);
	///@brief Virtual function to calculate temperature.
	virtual double calculate() = 0;
};

