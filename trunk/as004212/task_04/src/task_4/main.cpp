///@file
///@author Kolosun Kristian Aleksandrovich
///@date 11.11.2016

#include "head.h"
#include "TP.h"
#include "LTPson.h"
#include "NTPson.h"

using namespace std;

///@mainpage PID controller
///@image html linear_graph.png
///@details Graphic of linear temperature changes.
///@image html nonlinear_graph.png
///@details Graphic of nonlinear temperature changes.
///@brief Looking at the chart, you will notice that the linear model is adjusted more slowly than non - linear

int main()
{
	LinearTemperatureChange line;
	NonlinearTemperatureChange NoLine;
	line.show();
	NoLine.show();
	system("pause");
	return 0;
}