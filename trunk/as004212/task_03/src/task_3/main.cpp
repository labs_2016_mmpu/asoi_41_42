/**
@mainpage  A console application of linear and nonlinear temperature changes.
@file
*\image html linear_graph.png
*\details Graphic of linear temperature changes.
*\image html nonlinear_graph.png
*\details Graphic of nonlinear temperature changes.
@author Kristian Kolosun
@date 21.10.2016
*/

#include "head.h"
#include "TP.h"
#include "LTPson.h"
#include "NTPson.h"

using namespace std;
///@brief Basic abstract class.

///@brief  Class, which show the model of linear changes.

///@brief  Class, which show the model of nonlinear changes.

///@brief Function, which create objects of the classes and makes methods of them.
int main()
{
	LinearTemperatureChange line;
	NonlinearTemperatureChange NoLine;
	line.show();
	NoLine.show();
	system("pause");
	return 0;
}