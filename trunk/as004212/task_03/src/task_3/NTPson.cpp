#include "NTPson.h"
#include "head.h"


using namespace std;
void NonlinearTemperatureChange ::  show()
{
	cout << "\nNonlinear change:" << endl;
	cout << "Y         | T" << endl;
	Y[0] = 0;
	cout.width(10);
	cout << left << Y[0] << "| 1" << endl;
	Y[1] = 0.9*Y[0] + Ut + sin(Ut);
	cout.width(10);
	cout << left << Y[1] << "| 2" << endl;
	for (int i = 2; i < 20; i++)
	{
		Y[i] = 0.9*Y[i - 1] - 0.001*pow(Y[i - 2], 2) + Ut + sin(Ut);
		cout.width(10);
		cout << left << Y[i] << "| " << i + 1 << endl;
	}
}