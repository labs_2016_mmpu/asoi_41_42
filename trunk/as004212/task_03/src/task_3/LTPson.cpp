#include "LTPson.h"
#include "head.h"


using namespace std;
void LinearTemperatureChange :: show()
{
	cout << "Linear change:" << endl;
	cout << "Y         | T" << endl;
	Y[0] = 0;
	cout.width(10);
	cout << left << Y[0] << "| 1" << endl;
	for (int i = 1; i < 20; i++)
	{
		cout.width(10);
		Y[i] = 0.988*Y[i - 1] + 0.232*Ut;
		cout << left << Y[i] << "| " << i + 1 << endl;
	}
}
