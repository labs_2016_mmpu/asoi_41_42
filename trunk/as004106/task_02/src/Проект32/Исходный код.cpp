///@file
///@author Verishko Roman
///@04.11.2016

#include <iostream>

///@brief Output "Hello world on console"

int main()
{
		///@brief Output "Hello world"
	std::cout << "Hello world!\n";
	///@brief Wait if user press on any button
	system("pause");
}