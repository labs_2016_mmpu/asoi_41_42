﻿/**
@file main.cpp
@author Пархач Д.С.
@date 20.11.2016
*/
#include<iostream>
#include"GenModel.h"
#include"LinModel.h"
#include"NlinModel.h"
#include"PIDReg.h"

using namespace std;
/**
@brief Содержит моделирование ПИД-регулятора.
Выводит на консоль выходную температуру
*/
int main()
{
	setlocale(LC_ALL, "RUS");
	const double w = 15;

	LinModel *linMod = new LinModel();
	NlinModel *nonlinMod = new NlinModel(0);
	PIDReg *regul = new PIDReg(0.01, 0.5, 0.21, 0.5);

	cout << "Линейная модель: " << endl;

	double y = linMod->Model(0, 0);
	double u = regul->Deviation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = linMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	cout << "Нелинейная модель: " << endl;

	y = nonlinMod->Model(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = nonlinMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	system("PAUSE");
	return 0;
}