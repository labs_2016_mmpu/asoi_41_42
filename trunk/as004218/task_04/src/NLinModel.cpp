﻿#include "NlinModel.h"
#include<iostream>
#include<math.h>

NlinModel::NlinModel()
{
}

NlinModel::NlinModel(double y0)
{
	this->ytPrev = y0;
}

double NlinModel::Model(double yt, double ut)
{
	double resYt = 0;
	resYt = 0.9*yt - 0.001*ytPrev*ytPrev + ut + sin(ut);
	ytPrev = yt;
	return resYt;
};
NlinModel::~NlinModel()
{
}