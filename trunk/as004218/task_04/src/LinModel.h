﻿#pragma once
#include "GenModel.h"
///@brief дочерний класс, реализующий линейную модель
class LinModel :
	public GenModel
{
public:
	LinModel();
	///@brief выводит на консоль входное значение температуры на каждой итерации(Линейная модель)
	///@param yt - начальное входное значение температуры
	///@param ut - входное значение интенсивтости
	///@return yt 
	double Model(double yt, double ut);
	~LinModel();
};