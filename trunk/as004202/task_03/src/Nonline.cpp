#include "Nonline.h"
#include <iostream>

///@brief Nonline method solution
void NonLine::modeling() {
	double OldTemp = StartTemp;

	for (int i = 0; i < size; i++)
	{
		if (i > 0)
			OldTemp = Temp[i - 1];
		StartTemp = 0.9*StartTemp - 0.001*OldTemp + ChangeTemp + sin(ChangeTemp);
		Temp[i] = StartTemp;
	}
}
///@brief Nonline output