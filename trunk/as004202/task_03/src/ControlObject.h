#pragma once
#include <iostream>
#include <cmath> 
#include <iomanip> 
using namespace::std;

///@brief Abstract class
class ControlObject
{
protected:
	const int size = 20;
	double *Temp = new double[size];
public:
	double ChangeTemp = 20;
	double StartTemp = 1;
	void show()
	{
		for (int i = 1; i<size; i++)
		{
			cout << setw(3) << i << ": " << Temp[i] << endl;
		}
	}
	virtual void modeling() = 0;
	ControlObject()
	{
		for (int i = 0; i<size; i++)
			Temp[i] = 0;
	}

};