#include "Nonline.h"
#include <iostream>

///@brief Nonline method solution
void NonLine::modeling() {
	for (int i = 2; i < size; i++)
	{
		PidController(i);
		Temp[i] = 0.9*Temp[i-1] - 0.001*Temp[i-2] * Temp[i-2] + u + sin(u);
	}
}
///@brief Nonline output