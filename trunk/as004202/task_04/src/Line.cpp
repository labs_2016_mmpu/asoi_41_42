#include "Line.h"
#include <iostream>

///@brief Line method solution 
void line::modeling() {
	for (int i = 1; i<size; i++)
	{
		PidController(i);
		Temp[i] = 0.988*Temp[i-1] + 0.232*u;
	}
}
///@brief Line output 
