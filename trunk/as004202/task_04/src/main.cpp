///@File
///@Author Vakulyuk Vlad
///@18.11.2016
#include "Line.h"
#include "Nonline.h"
#include <iostream>
using namespace::std;
///@mainpage Graphs of Temperature 
///@image html Line.png
///@image html Nonline.png
int main()
{
	line q1;
	q1.modeling();
	cout << "Line" << endl;
	q1.show();
	cout << endl << endl << endl << endl;
	NonLine Q1;
	cout << "Nonline" << endl;
	Q1.modeling();

	Q1.show();
	system("pause");
	return 0;
}