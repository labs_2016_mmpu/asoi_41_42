#include "stdafx.h"
#include "temp.h"
///@brief calculate linear function of temperature change
void Line::temp()
{
	int t = 0;
	cout << "linear function" << endl;
	double  y_line = 0;
	for (int t = 0; t < 6; t++)
	{
		PID();
		y_line = 0.988 * y + 0.232 * u;
		y = y_line;
		cout << "\t" << y_line << "\t" << u << endl;

	}

}
///@brief calculate nonlinear function of temperature change
void nonLine::temp()
{
	int t = 0;
	double y_nline = 0, y1 = 0;
	cout << "nonlinear function" << endl;
	for (t = 0; t < 6; t++) {
		PID();
		y_nline = 0.988*y1 - 0.001*y*y + u + sin(u);
		y1 = y_nline;
		y = y1;
		cout << "\t" << y_nline << "\t" << u << endl;

	}

}
