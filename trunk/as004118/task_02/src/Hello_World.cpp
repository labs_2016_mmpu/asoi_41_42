/**
@maipage It is simple console application that display message "Hello, World!"
@file
@author Marchuk Roman
@date 24.10.2016
*/
#include "stdafx.h"
#include <iostream>
/**
@brief Simple and single main function
*/
int main() {
	/**
	@brief Display the message
	*/
	std::cout << "Hello, World!" << std::endl;
	return 0;
}