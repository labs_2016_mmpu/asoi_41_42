﻿///@file Nonlinear.h содержит описание для нелинейной модели

#pragma once
#include "stdafx.h"
#include "Abstract.h"
#ifndef NONLINEAR_H
#define NONLINEAR_H

/**@class Nonlinear
Наследуем класс от базового(абстрактного)
*/

class Nonlinear : public Abstract {
public:
	///@briefКонструктор по умолчанию
	Nonlinear();
	///@briefДеструктор
	~Nonlinear();
	///@brief Метод, который выводит информацию о выходной температуре
	void output();
};

#endif
