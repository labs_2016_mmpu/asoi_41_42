﻿///@file Nonlinear.cpp  содержит реализацию для нелинейной модели

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "Nonlinear.h"

Nonlinear::Nonlinear() {}

Nonlinear::~Nonlinear() {}
///@brief Реализация метода вывода информации для нелинейной модели
void Nonlinear::output() {
	y[0] = 0;
	y[1] = 0;
	std::cout << "\nNonlinear\n\n";
	std::cout.width(8);
	std::cout << "y" << "  |  " << "T\n";
	for (int i = 1; i <= 20; i++) {
		y[i + 1] = 0.9*y[i] - 0.001*y[i - 1] * y[i - 1] + u + sin(u);
		std::cout.width(8);
		std::cout << y[i] << "  |  " << i << std::endl;
	}
}
