#pragma once
#include "stdafx.h"
#include <iostream>
#include <cmath>
using namespace std;

///@brief abstractny class 
class AbstrCl {
public:

	double u = 10;
	double y[10];
	virtual void temp() = 0;
};


///@brief class nonlinear_model
///@image html nonlinear.png
///@brief calculate nonlinear function
class nonLine :public AbstrCl
{
public:
	
	void temp();
};

///@brief class linear_model 
///@image html linear.png
///@brief calculate linear function
class Line :public AbstrCl
{
public:
	
	void temp();
};

