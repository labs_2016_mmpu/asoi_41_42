/**
@file
@author Vasiliuk Kirill
@date 09.12.2016
@mainpage Temperature models
@image html Line.png
@image html Noline.png
*/
#include <iostream>
#include <cstdlib>
#include "Abstract.h"
#include "Line.h"
#include "Noline.h"
using namespace std;

///@brief entry point of the program
int main()
{
	Line lin;
	Noline nonlin;
	///@brief creating and displaying parametrs of linear model
	lin.show();
	cout << endl;
	///@brief creating and displaying parametrs of nonlinear model
	nonlin.show();
	system("pause");
	return 0;
};