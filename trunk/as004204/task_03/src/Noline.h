#pragma once
#include "Abstract.h"
using namespace::std;
///@class Noline
///@brief Class Noline derived from Abstract 
class Noline :public Abstact
{
private:
	double array_y[11] = {};
public:
	Noline() :array_y() {}

	///@brief Noline method solution 
	void ShowResult();
};
