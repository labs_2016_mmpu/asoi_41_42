#include <stdlib.h>
#include "models.h"


void Model::append_inp(double in){
  inp.push_back(in);
}

size_t Model::get_inp_size(){
  return inp.size();
}

void Model::set_inp(vector<double>& inp){
  this->inp = inp;
}

vector<double>& Model::get_inp() {
  return inp;
}

#define TASK_04_PATCH

void LinModel::calculate(){
  if(get_inp_size() == 0){
    fprintf(stderr,"imput data is empty\n");
    abort();
  }
#ifndef TASK_04_PATCH
  for(vector<double>::iterator it = inp.begin(); it != inp.end(); it++){
    out.push_back(0.998*out.back() + 0.232*(*it));
  }
#else
  for(int i = 0; i < 11; ++i){
    out.push_back(0.998*out.back() + 0.232*inp.back());
    pidErrorUpdate(&pidc, out.back());
    inp.push_back(getPidOutput(&pidc, inp.back()));
  }
#endif
  out.erase(out.begin());
}

void LinModel::print(){
  size_t outlen = out.size();
  for(size_t i = 0; i < outlen; i++){
    std::cout << inp[i] << "\t" << out[i] << "\n";
  }
}


void NonLinModel::calculate(){
  if(get_inp_size() == 0){
    fprintf(stderr,"imput data is empty\n");
    abort();
  }

#ifdef TASK_04_PATCH
  for(int i = 0; i < 11; ++i){
    out.push_back(0.9*out.back()
        - 0.001*std::pow( out[out.size()-2], 2 )
        + inp.back()
        + std::sin( inp[inp.size()-2] )
        );
    pidErrorUpdate(&pidc, out.back());
    inp.push_back(getPidOutput(&pidc, inp.back()));
  }
#else
  for(vector<double>::iterator it = inp.begin(); it != inp.end(); it++){
    out.push_back(0.9*out.back()
        - 0.001*std::pow( out[out.size()-2], 2 )
        + inp.back()
        + std::sin( inp[inp.size()-2] )
        );
  }
#endif
  out.erase(out.begin());
}

void NonLinModel::print(){
  size_t outlen = out.size();
  for(size_t i = 0; i < outlen; i++){
    std::cout << inp[i] << "\t" << out[i] << "\n";
  }
}
