/** \file models.h
 *  \brief Файл содержит классы и методы реализующие
 *  концепцию объекта управления
 * */

#ifndef MODELS_H
#define MODELS_H

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>

using std::vector;

//#ifdef TASK_04_PATCH
#include "../../task_04/src/pidcontroller.h"
//#endif


/**
 * \brief представляет модель управления и
 * методы управления ей: установка входных параметров,
 * получение выходных, печать входных/выходных данных.
 */
class Model{
  public:
    /// входные параметры должны быть установлены, до использования этого метода.
    virtual void calculate() = 0;

    /// печать входных/выходных параметров
    virtual void print() = 0;

    /// добавление нового входного параметра.
    virtual void append_inp(double in);

    virtual size_t get_inp_size();

    /// установка нового набора выходных параметров
    virtual void set_inp(vector<double>& inp);

    virtual vector<double>& get_inp();
    virtual ~Model(){}
  protected:
    vector<double> inp;
};

/** \brief реализует концепцию линейной модели управления
 *  \image html lineal.png
 */
class LinModel : public Model{
  public:
    void calculate();
    void print();
    LinModel(){out.push_back(0.0);}
    ~LinModel() {};
  private:
    /// выходные параметры
    vector<double> out;
};

/**
 * \brief реализует концепцию нелинейной модели управления
 * \image html nonlineal
 */
class NonLinModel : public Model{
  public:
    void calculate();
    void print();
    NonLinModel(){out.push_back(0.0);}
    ~NonLinModel() {};
  private:
    vector<double> out;
};

#endif //MODELS_H
