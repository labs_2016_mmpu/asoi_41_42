#include "pidcontroller.h"

void pidErrorUpdate(PidController* ppidc, double current_output){
  ppidc->e0 = ppidc->W - current_output; 
  ppidc->e1 = ppidc->e0;
  ppidc->e2 = ppidc->e1;
}

double getPidOutput(PidController* ppidc, double previous_po){
 return previous_po + ppidc->q0 * ppidc->e0
                    + ppidc->q1 * ppidc->e1
                    + ppidc->q2 * ppidc->e2;
}


