/**\file pidcontroller.h 
 * \brief содержит стуктуру и функции для реализации
 *  концепции ПИД-контроллера. 
 *
 */  
#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H


#define TASK_04_PATCH

/** \brief переменные для расчета pid_controller
 *  \image html lineal.png
 *  \image html nonlineal.png
 */  
typedef struct pid_controller{
  const double T0 = 0.1;
  const double Td = 0.1;
  const double T = 0.1;
  const double K = 0.1;

  const double W = 24;

  const double q0 = K*(1.0 + Td/T0);
  const double q1 = (-1)*K*(1.0 + 2.0*Td/T0 - T0/T);
  const double q2 = K*Td/T0;

  double e0 = 0;
  double e1 = 0;
  double e2 = 0;

}PidController;

extern PidController pidc;

void pidErrorUpdate(PidController* ppidc, double current_output);
double getPidOutput(PidController* ppidc, double previous_po);



#endif
