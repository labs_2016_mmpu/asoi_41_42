/** \file main.cpp
 *  \author Юхно Владислав (as004223)
 *  \brief 
 */

#define TASK_04_PATCH

#include <iostream>
#include <cstring>
#include "pidcontroller.h"
#include "../../task_03/src/models.h"
/// для ifdefine конструкций в task_03/src/*



// **ghm**
PidController pidc;
/**
 *
 * \image html scr01.png
 * \image html scr02.png
 * @param argc кол-во аргументов переданные программе, включая путь
 * @param argv массив строк содержащий эти аргументы
 * @return 0
 */
int main(int argc, char** argv){
  LinModel lm;
  NonLinModel nlm;

  lm.append_inp(0);
  nlm.append_inp(0);

  if(argc > 1 && strcmp(argv[1],"ln") == 0 ){
    lm.calculate();
    lm.print();
  }else if(argc > 1 && strcmp(argv[1],"nln") == 0){
    nlm.calculate();
    nlm.print();
  }
  else{
    fprintf(stderr, "First arg must be 'ln' or 'nln' \n");
  }
  

  return 0;
}
