/** \file LinearModel.cpp
*  \author Karpovich Vladislav (as004111)
*  \brief Implementation of the LinearModel class.
*/
#include "LinearModel.h"

///@brief Implement constructor with paramethers.
LinearModel::LinearModel(double inputValue, double outputValue) :TemperatureSystem(inputValue,outputValue){}

///@brief Implement method calculate.
double LinearModel::calculate()
{
	systemOutputValue = 0.988*systemOutputValue + 0.232*systemInputValue;
	return systemOutputValue;
}
