#include "TemperatureModel.h"
#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

TemperatureModel::TemperatureModel(double _incoming_heat, unsigned int _number_of_calc){
    incoming_heat = _incoming_heat;
    number_of_calc = _number_of_calc;
}
void TemperatureModel::changestartvalue(double _start_value){
    incoming_heat = _start_value;
}
void TemperatureModel::changecalcnumber(unsigned int _new_calc_number){
    number_of_calc = _new_calc_number;
}
double TemperatureModel::getstartvalue(void){
    return incoming_heat;
}
unsigned int TemperatureModel::getcalcnumber(void){
    return number_of_calc;
}
void TemperatureModel::printmodel(void){
    cout << "The incoming heat: " << incoming_heat << ";" << endl <<
    "Number of calculations: " << number_of_calc << ";" << endl;
}

LinearModel::LinearModel(double _incoming_heat, unsigned int _number_of_calc) : 
    TemperatureModel(_incoming_heat, _number_of_calc){}
void LinearModel::printmodel(void){
    TemperatureModel::printmodel();
    cout << "Linear model table:" << endl;
    
    const double incoming_Heat_Const {0.232 * incoming_heat};
    double y {incoming_Heat_Const};

    for(unsigned int i {1}; i <= number_of_calc; i++){
        cout << "y[" << i << "] = " << y << ";" << endl;
        y = 0.988 * y + incoming_Heat_Const;
    }
}

NonLinearModel::NonLinearModel(double _incoming_heat, unsigned int _number_of_calc) :
    TemperatureModel(_incoming_heat, _number_of_calc){}
void NonLinearModel::printmodel(void){
    TemperatureModel::printmodel();
    cout << "Nonlinear model table:" << endl;

    const double incoming_Heat_Const {incoming_heat + sin(incoming_heat)};
    double y_1 {0};
    double y_2 {0};
    double y_3 {incoming_Heat_Const};

    for(unsigned int i {1}; i <= number_of_calc; i++){
        cout << "y[" << i << "] = " << y_3 << ";" << endl;
        y_1 = y_2;
        y_2 = y_3;
        y_3 = 0.9 * y_2 - 0.001 * pow(y_1, 2) + incoming_Heat_Const;
    }
}