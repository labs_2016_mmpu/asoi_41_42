#ifndef TASK_03_TEMPERATUREMODEL_H
#define TASK_03_TEMPERATUREMODEL_H

class TemperatureModel{
protected:
    double incoming_heat; 
    unsigned int number_of_calc; 
public:
    TemperatureModel(double, unsigned int);
    void changestartvalue(double);
    void changecalcnumber(unsigned int);
    double getstartvalue(void);
    unsigned int getcalcnumber(void);
    virtual void printmodel(void) = 0;
};

class LinearModel : public TemperatureModel{
public:
    LinearModel(double, unsigned int = 10);
    virtual void printmodel(void);
};

class NonLinearModel : public TemperatureModel{
public:
    NonLinearModel(double, unsigned int = 10);
    virtual void printmodel(void);
};

#endif
