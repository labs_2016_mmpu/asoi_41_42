#include "TemperatureModel.h"
#include "USRCustomFunction.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

void usermenu(LinearModel *, NonLinearModel *);
void newheatvalue(TemperatureModel *);
void newoutputvalue(TemperatureModel *);

int main(){

    double heat_value_linear;
    unsigned int calc_linear;

    ClearScreen();
    cout << "Enter the amount of heat entering to the system (linear model): ";
    cin >> heat_value_linear;
    cout << "Enter the desired number of output values (linear model): ";
    cin >> calc_linear;
    cout << endl;   

    LinearModel *linear_obj = new LinearModel(heat_value_linear, calc_linear);
    double heat_value_non_linear;
    unsigned int calc_non_linear;

    cout << "Enter the amount of heat entering to the system (nonlinear model): ";
    cin >> heat_value_non_linear;
    cout << "Enter the desired number of output values (nonlinear model): ";
    cin >> calc_non_linear;
    cout << "Data added." << endl;

    NonLinearModel *non_linear_obj = new NonLinearModel(heat_value_non_linear, calc_non_linear); 

    usermenu(linear_obj, non_linear_obj);

    delete linear_obj;
    delete non_linear_obj;
    return 0;
} 

void usermenu(LinearModel *_linear_obj, NonLinearModel *_non_linear_obj){
    char ch;
    while(ch != 'Q' && ch != 'q'){
        cout << endl <<
        "1. Print data of the linear model." << endl << 
        "2. Print data of the nonlinear model." << endl << 
        "3. Set a new value for the amount of heat of the linear model." << endl << 
        "4. Set a new value for the amount of heat of the nonlinear model." << endl << 
        "5. Set the new number of output values for the linear model." << endl << 
        "6. Set the new number of output values for the nonlinear model." << endl << 
        "Q. Exit." << endl << 
        "Choice operation: ";
        cin.ignore();
        cin.get(ch);
        cout << endl;
        switch(ch){
            case '1':{
                ClearScreen();
                _linear_obj -> printmodel();
                break;
            }
            case '2':{
                ClearScreen();
                _non_linear_obj -> printmodel();
                break;
            }
            case '3':{
                newheatvalue(_linear_obj);
                break;
            }
            case '4':{
                newheatvalue(_non_linear_obj);
                break;
            }
            case '5':{
                newoutputvalue(_linear_obj);
                break;
            }
            case '6':{
                newoutputvalue(_non_linear_obj);
                break;
            }
            case 'Q':
            case 'q':{
                break;
            }
            default:{
                ClearScreen();
                cout << "Uncorrected input!" << endl;
                break;
            }
        }
    }
}
void newheatvalue(TemperatureModel *_changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getstartvalue() << endl << 
    "Enter the new value of the heat: ";
    
    double temp_value;

    cin >> temp_value;
    _changed_value -> changestartvalue(temp_value);
    cout << "Done!" << endl;
}
void newoutputvalue(TemperatureModel *_changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getcalcnumber() << endl << 
    "Enter the new number of output values: ";

    unsigned int temp_value;

    cin >> temp_value;
    _changed_value -> changecalcnumber(temp_value);
    cout << "Done!" << endl;
}