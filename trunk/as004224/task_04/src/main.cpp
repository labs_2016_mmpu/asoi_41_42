#include "TemperatureModel.h"
#include "USRCustomFunction.h"
#include <iostream>
#include <cctype>
#include <fstream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::ofstream;
using std::string;

void userMenu(LinearModel *, NonLinearModel *);
void newTempeartureValue(TemperatureModel *);
void newOutputValue(TemperatureModel *);
void newPIDCoefficients(TemperatureModel *);
void printDataToFile(TemperatureModel *, const string &);


int main(){

    double temperature_value;
    unsigned int calc_value;

    ClearScreen();
    cout << "Enter the desired temperature value (linear model): ";
    cin >> temperature_value;
    cout << "Enter the desired number of output values (linear model): ";
    cin >> calc_value;
    cout << endl;   

    LinearModel *linear_obj = new LinearModel(temperature_value, calc_value);

    cout << "Enter the desired temperature value (nonlinear model): ";
    cin >> temperature_value;
    cout << "Enter the desired number of output values (nonlinear model): ";
    cin >> calc_value;
    cout << "Data added." << endl;

    NonLinearModel *non_linear_obj = new NonLinearModel(temperature_value, calc_value); 

    userMenu(linear_obj, non_linear_obj);

    delete linear_obj;
    delete non_linear_obj;

    return 0;
} 

void userMenu(LinearModel * _linear_obj, NonLinearModel * _non_linear_obj){

    char ch;
    
    while(ch != 'Q'){
        cout << endl <<
        "1. Print data of the linear model (using PID controller)." << endl << 
        "2. Print data of the nonlinear model (using PID controller)." << endl << endl <<

        "3. Set a new value for the desired temperature of the linear model." << endl << 
        "4. Set a new value for the desired temperature of the nonlinear model." << endl << 
        "5. Set the new number of output values for the linear model." << endl << 
        "6. Set the new number of output values for the nonlinear model." << endl << endl <<
        
        "7. Set a new value for PID controller coefficients (linear model)." << endl <<
        "8. Set a new value for PID controller coefficients (nonlinear model)." << endl << endl <<

        "L. Write to file the values of the linear model." << endl <<
        "N. Write to the file of the nonlinear model." << endl <<
        "Q. Exit." << endl << 
        "Choice operation: ";
        cin.ignore().get(ch);
        ch = toupper(ch);
        cout << endl;
        
        switch(ch){
            case '1':{
                ClearScreen();
                _linear_obj -> printModel(cout);
                break;
            }
            case '2':{
                ClearScreen();
                _non_linear_obj -> printModel(cout);
                break;
            }
            case '3':{
                newTempeartureValue(_linear_obj);
                break;
            }
            case '4':{
                newTempeartureValue(_non_linear_obj);
                break;
            }
            case '5':{
                newOutputValue(_linear_obj);
                break;
            }
            case '6':{
                newOutputValue(_non_linear_obj);
                break;
            }
            case '7':{
                newPIDCoefficients(_linear_obj);
                break;
            }
            case '8':{
                newPIDCoefficients(_non_linear_obj);
                break;
            }
            case 'L':{
                printDataToFile(_linear_obj, "LinearModelData");
                break;
            }
            case 'N':{
                printDataToFile(_non_linear_obj, "NonLinearModelData");
                break;
            }
            case 'Q':{
                break;
            }
            default:{
                ClearScreen();
                cout << "Uncorrected input!" << endl;
                break;
            }
        }
    }
}
void newTempeartureValue(TemperatureModel * _changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getStartValue() << endl << 
    "Enter the new value of the temperature: ";
    
    double temp_value;

    cin >> temp_value;
    _changed_value -> changeStartValue(temp_value);
    cout << "Done!" << endl;
}
void newOutputValue(TemperatureModel * _changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getCalcNumber() << endl << 
    "Enter the new number of output values: ";

    unsigned int temp_value;

    cin >> temp_value;
    _changed_value -> changeCalcNumber(temp_value);
    cout << "Done!" << endl;
}
void newPIDCoefficients(TemperatureModel * _changed_value){
    ClearScreen();
    cout << "Current transmission value (K): " << _changed_value -> getTransmissionValue() << endl << 
    "Current integration value (T): " << _changed_value -> getIntegrationValue() << endl <<
    "Current differentiation value (Td): " << _changed_value -> getDifferentiationValue() << endl << endl <<
    "Enter the new transmisson value (K): ";

    float temp;

    cin >> temp;
    _changed_value -> changeTransmissionValue(temp);
    cout << "Enter the new integration value (T): ";
    cin >> temp;
    _changed_value -> changeIntegrationValue(temp);
    cout << "Enter the new differentiation value (Td): ";
    cin >> temp;
    _changed_value -> changeDifferentiationValue(temp);
    cout << "Done!" << endl;
}
void printDataToFile(TemperatureModel * _printing_value, const string & _filename){ 
    ClearScreen();

    ofstream output_file(_filename);
                
    _printing_value -> printModel(output_file, true);
    output_file.close();
    cout << "File successfully created!" << endl;
}
