/**
\file
\brief Программа моделирования работы ПИД-регулятора на языке C++.
\authors Яцевич Игорь Николаевич, группа АС-42.
\date 18.11.2016
\warning В программе не реализована защита от ввода некорректных
данных, пожалуйста, следите за типом вводимых данных.

Программа, написанная на объекто-ориентированном языке C++.
Система сборки проекта - СMake. Основной целью реализации 
данной программы, является программная реализация и последующая 
настройка ПИД-регулятора для двух представленных моделей объектов: 
линейной и нелинейной. 

Линейная модель объекта управления:
\f[y_{t+1} = 0.988y_{t} + 0.232u_{t}\f]

Нелинейная модель объекта управления:
\f[y_{t+1} = 0.9y_{t} - 0.001y^2_{t-1} + u_{t} + sin(u_{t})\f]

В ходе написания данной программы подразумевается использование
ООП, создается абстрактный базовый класс (TemperatureModel), 
имеющий какие-либо общие черты для последующих классов-наследников. 
А именно: необходимая температура, количество выходных значений, 
а также, коэффициенты для управления ПИД-регулятором (по умолчанию 
равные 0.1). В последствии возможно конфигурирование.
После чего, выполняется наследование данного класса, классами,
описывающими линейную (LinearModel) и нелинейную (NonLinearModel)
модели управления.

Пример работы программы:
Для линейной и нелинейной модели объекта управления зададим 
начальное значение температуры, равное 30, и количество выходных 
значений равным 100.

Заполнение данных о моделях управления:
![Заполнение данных.](enter_data.png)

Пример пользовательского меню в программе:
![Выбор операций.](user_menu.png)

\mainpage Работа с ПИД-регулятором в данной программе.

График результата работы ПИД-регулятора для линейного объекта:
![График линейной модели.](linear_model_graph.png)
Коэффициенты ПИД-регулятора:
\f[K = 0.5, T = 0.9, T_{D} = 0.05\f]

График результата работы ПИД-регулятора для нелинейного объекта:
![График нелинейной модели.](nonlinear_model_graph.png)
Коэффициенты ПИД-регулятора:
\f[K = 0.5, T = 0.6, T_{D} = 0.02\f]

Исходя из полученных графиков можно видеть влияние настройки ПИД-регулятора 
на конечный график.
*/
#include "TemperatureModel.h"
#include "USRCustomFunction.h"
#include <iostream>
#include <cctype>
#include <fstream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::ofstream;
using std::string;

/**
Функция, отвечающая за вывод пользовательского меню и последующие
манипуляции с объектами классов LinearModel и NonLinearModel.
Выводит список доступных пользователю функций, для манипуляций с 
моделями. К таким функциям относится:
- Вывод информации линейного/нелинейного объекта управления,
и соответственной работы ПИД-регулятора.
- Установка нового значения температуры для линейного/нелинейного объекта 
управления управления.
- Установка нового количества выходных значений для линейного/нелинейного 
объекта управления.
- Установка новых коэффициентов для ПИД-регулятора соответствующего 
объекта.
- Вывод информации о работе ПИД-регулятора конеретного объекта в 
текстовый файл (используется для построения графиков в онлайн-конструкторе).
- Выход из программы.

Осуществлении навигации по пользовательскому меню реализуется при
помощи чтения из потока ввода соттветствующих команд, введенных
пользователем.

\param[in,out] _linear_obj Указатель на объект для манипуляции, 
класса LinearModel.
\param[in,out] _non_linear_obj Указатель на объект для манипуляции,
класса NonLinearModel.
*/
void userMenu(LinearModel *, NonLinearModel *);

/**
Функция, отвечающая за установку нового значения температуры,
поступающего в смоделированную систему. При выполнении данной функции
выводится консольное приглашение на ввод нового значения температуры,
полученное значение связывается с соответствующим объектом.
Тип вводимых данных - double.

\param[in,out] _changed_value Указатель на объект для манипуляции, 
класса TemperatureModel. Реализуется принцип полиморфизма.
*/
void newTempeartureValue(TemperatureModel *);

/**
Функция, отвечающая за установку нового количества выходных
значений для выбранного объекта. При выполнении данной функции
выводится консольное приглашение на ввод нового значения
количества выходных значений, полученное значение связывается 
с соответствующим объектом.
Тип вводимых данных - unsigned int.

\param[in,out] _changed_value Указатель на объект для манипуляции, 
класса TemperatureModel. Реализуется принцип полиморфизма.
*/
void newOutputValue(TemperatureModel *);

/**
Функция, отвечающая за установку новых коэффициентов ПИД-регулятора 
для линейного/нелинейного объекта управления. Используется для 
регулировки работы ПИД-регулятора. Изменяемые коэффициенты:
\f[K, T, T_{D}\f]

\param[in,out] _changed_value Указатель на объект для манипуляции, 
класса TemperatureModel. Реализуется принцип полиморфизма.
*/
void newPIDCoefficients(TemperatureModel *);

/**
Функция, отвечающая за вывод информации о результате работы 
ПИД-регулятора в текстовый файл. Текстовый файл в последствии 
используется для построения графика при помощи онлайн-конструктора.

\param[in,out] _printing_value Указатель на объект для манипуляции, 
класса TemperatureModel. Реализуется принцип полиморфизма.
\param[in] _filename Имя создаваемого файла (LinearModelData - для 
линейного объекта управления, NonLinearModelData - для нелинейного 
объекта управления). 
*/
void printDataToFile(TemperatureModel *, const string &);

/**
Основная функция 'main' написанной программы, выполняет 
первоначальное чтение данных для построения соответствующих
объектов управления и последующего регулирования ПИД-регулятором. 
После успешного чтения даннных вызывает функцию для вывода 
пользовательского меню для выбора дальнейших действий.

\return Нулевое значение (в случае успешного завершения).
*/
int main(){

    double temperature_value;
    unsigned int calc_value;

    ClearScreen();
    cout << "Enter the desired temperature value (linear model): ";
    cin >> temperature_value;
    cout << "Enter the desired number of output values (linear model): ";
    cin >> calc_value;
    cout << endl;   

    LinearModel *linear_obj = new LinearModel(temperature_value, calc_value);

    cout << "Enter the desired temperature value (nonlinear model): ";
    cin >> temperature_value;
    cout << "Enter the desired number of output values (nonlinear model): ";
    cin >> calc_value;
    cout << "Data added." << endl;

    NonLinearModel *non_linear_obj = new NonLinearModel(temperature_value, calc_value); 

    userMenu(linear_obj, non_linear_obj);

    delete linear_obj;
    delete non_linear_obj;

    return 0;
} 

void userMenu(LinearModel * _linear_obj, NonLinearModel * _non_linear_obj){

    char ch;
    
    while(ch != 'Q'){
        cout << endl <<
        "1. Print data of the linear model (using PID controller)." << endl << 
        "2. Print data of the nonlinear model (using PID controller)." << endl << endl <<

        "3. Set a new value for the desired temperature of the linear model." << endl << 
        "4. Set a new value for the desired temperature of the nonlinear model." << endl << 
        "5. Set the new number of output values for the linear model." << endl << 
        "6. Set the new number of output values for the nonlinear model." << endl << endl <<
        
        "7. Set a new value for PID controller coefficients (linear model)." << endl <<
        "8. Set a new value for PID controller coefficients (nonlinear model)." << endl << endl <<

        "L. Write to file the values of the linear model." << endl <<
        "N. Write to the file of the nonlinear model." << endl <<
        "Q. Exit." << endl << 
        "Choice operation: ";
        cin.ignore().get(ch);
        ch = toupper(ch);
        cout << endl;
        
        switch(ch){
            case '1':{
                ClearScreen();
                _linear_obj -> printModel(cout);
                break;
            }
            case '2':{
                ClearScreen();
                _non_linear_obj -> printModel(cout);
                break;
            }
            case '3':{
                newTempeartureValue(_linear_obj);
                break;
            }
            case '4':{
                newTempeartureValue(_non_linear_obj);
                break;
            }
            case '5':{
                newOutputValue(_linear_obj);
                break;
            }
            case '6':{
                newOutputValue(_non_linear_obj);
                break;
            }
            case '7':{
                newPIDCoefficients(_linear_obj);
                break;
            }
            case '8':{
                newPIDCoefficients(_non_linear_obj);
                break;
            }
            case 'L':{
                printDataToFile(_linear_obj, "LinearModelData");
                break;
            }
            case 'N':{
                printDataToFile(_non_linear_obj, "NonLinearModelData");
                break;
            }
            case 'Q':{
                break;
            }
            default:{
                ClearScreen();
                cout << "Uncorrected input!" << endl;
                break;
            }
        }
    }
}
void newTempeartureValue(TemperatureModel * _changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getStartValue() << endl << 
    "Enter the new value of the temperature: ";
    
    double temp_value;

    cin >> temp_value;
    _changed_value -> changeStartValue(temp_value);
    cout << "Done!" << endl;
}
void newOutputValue(TemperatureModel * _changed_value){
    ClearScreen();
    cout << "Current value: " << _changed_value -> getCalcNumber() << endl << 
    "Enter the new number of output values: ";

    unsigned int temp_value;

    cin >> temp_value;
    _changed_value -> changeCalcNumber(temp_value);
    cout << "Done!" << endl;
}
void newPIDCoefficients(TemperatureModel * _changed_value){
    ClearScreen();
    cout << "Current transmission value (K): " << _changed_value -> getTransmissionValue() << endl << 
    "Current integration value (T): " << _changed_value -> getIntegrationValue() << endl <<
    "Current differentiation value (Td): " << _changed_value -> getDifferentiationValue() << endl << endl <<
    "Enter the new transmisson value (K): ";

    float temp;

    cin >> temp;
    _changed_value -> changeTransmissionValue(temp);
    cout << "Enter the new integration value (T): ";
    cin >> temp;
    _changed_value -> changeIntegrationValue(temp);
    cout << "Enter the new differentiation value (Td): ";
    cin >> temp;
    _changed_value -> changeDifferentiationValue(temp);
    cout << "Done!" << endl;
}
void printDataToFile(TemperatureModel * _printing_value, const string & _filename){ 
    ClearScreen();

    ofstream output_file(_filename);
                
    _printing_value -> printModel(output_file, true);
    output_file.close();
    cout << "File successfully created!" << endl;
}
