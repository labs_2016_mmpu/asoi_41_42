/**
@mainpage  A console application that displays the "Hello World!"
@file ???
@author Krivol Vladislav
@date 28.10.2016
@brief We define an entry point in the program
*/

#include "stdafx.h"
#include <iostream>

int main()
{
	/**
	@brief Display message "Hello, world!"
	*/
	std::cout << "Hello, World!";
	return 0;
}