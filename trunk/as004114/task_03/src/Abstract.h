﻿///@file Abstract.h содержит описание абстрактного класса

#pragma once
#ifndef ABSTRACT_H
#define ABSTRACT_H

/**@class Abstract 
Абстрактный класс
*/

class Abstract{
public:
	///@briefМассив для хранения значений выхода системы в разные моменты времени
	float y[20];
	///@briefВход системы, постоянная температура
	float u = 10;
	///@briefКонструктор по умолчанию
	Abstract();
	///@briefДеструктор
	~Abstract();
protected:
	///@brief Абстрактный метод, который выводит информацию о выходной температуре
	virtual void output() = 0;
};

#endif
