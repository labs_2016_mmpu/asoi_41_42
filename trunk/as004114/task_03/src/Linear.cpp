﻿///@file Linear.cpp  содержит реализацию для линейной модели

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "Linear.h"

Linear::Linear() {}

Linear::~Linear() {}
///@brief Реализация метода вывода информации для линейной модели
void Linear::output() {
	std::cout << "Linear\n\n";
	std::cout.width(8);
	std::cout << "y" << "  |  " << "T\n";
	y[0] = 0;
	for (int i = 0; i <= 20; i++) {
		y[i + 1] = 0.988*y[i] + 0.232*u;
		std::cout.width(8);
		std::cout << y[i] << "  |  " << i << std::endl;
	}
}