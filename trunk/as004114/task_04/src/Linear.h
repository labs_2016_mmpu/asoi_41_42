///@file Linear.h сcontains a Linear class description
#pragma once
#include "Abstract.h"
#ifndef LINEAR_H
#define LINEAR_H

/**@class Linear 
Linear class inherited from Abstract class
*/

class Linear : public Abstract {
public:
	///@brief Default constructor
	Linear();
	///@brief Destructor
	~Linear();
	///@brief Output method for linear model
	void output();
};

#endif