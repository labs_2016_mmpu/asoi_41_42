#include "Parent.h"
#include "Linear.h"
using namespace::std;

///@brief Linear solution 
void Linear::ShowResult()
{
	for (int i = 1; i <= 20; i++)
	{
		PidController();
		y1 = 0.988*y + 0.232*u;
		y = y1;
		///@brief Linear output
		cout << i << "  |  " << y1 << "  |  " << u << endl;
	}
}