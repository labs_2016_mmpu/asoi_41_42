#pragma once
#include <iostream>

///@class Parent
///@brief Abstract class
class Parent
{
public:
	///@param Define the variables for the task
	double Ti = 0.34;
	double Td = 0.2;
	double K = 0.5;
	double T0 = 0.7;
	double e1 = 0;
	double e2 = 0;
	double e3 = 0;
	double q0 = 0;
	double q1 = 0;
	double q2 = 0;
	double u = 0;
	double w = 30;
	double y = 0;
	double y1 = 0;
	double y2 = 0;
	///@brief Functions for solution
	void Parent::PidController();
	virtual void ShowResult() = 0;
};