#pragma once
#include <iostream>

///@brief Abstract class
class Parent
{
private:
	double array_y[11] = {};
public:
	double ut = 10;
	Parent() :array_y() {};
	virtual void ShowResult() = 0;

};

