///@File
///@Author Zholneruk Dmitriy
///@04.11.2016
#include "Linear.h"
#include "Nonlinear.h"
#include <iostream>

using namespace::std;
///@param Input temperature 
const double ut = 10;

///@mainpage Graphs of Temperature 
///@image html Linear.png
///@image html Nonlinear.png
int main()
{
	Linear *ObjectLinear = new Linear;
	Nonlinear *ObjectNonlinear = new Nonlinear;
	ObjectLinear->ShowResult();
	ObjectNonlinear->ShowResult();
	system("pause");
	return 0;
}