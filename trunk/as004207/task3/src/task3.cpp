#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

class AbstactClass {
public:
	float Z[20];
	float p = 10;
protected:
	virtual void dannie() = 0;
};
class LinMod : public AbstactClass
{
public:
	void dannie()
	{

		cout << "LinMod\n\n";
		cout.width(8);
		cout << "Z" << "  |--|  " << "Temperaura\n";
		Z[0] = 0;
		for (int i = 0; i < 21; i++)
		{
			Z[i + 1] = 0.988*Z[i] + 0.232*p;
			cout.width(8);
			cout << Z[i] << "  |--|  " << i << endl;
		}
	}
};
class NeLinMod : public AbstactClass
{
public:
	void dannie()
	{
		Z[0] = 0;
		Z[1] = 0;
		cout << "\nNeLinMod\n\n";
		cout.width(8);
		cout << "Z" << "  |--|  " << "Temperatura\n";
		for (int i = 1; i < 21; i++)
		{
			Z[i + 1] = 0.9*Z[i] - 0.001*Z[i - 1] * Z[i - 1] + p + sin(p);
			cout.width(8);
			cout << Z[i] << "  |--|  " << i << endl;
		}
	}
};
int main(int argc, char* argv[])
{
	LinMod a;
	NeLinMod b;
	a.dannie();
	b.dannie();
	system("pause");
	return 0;
};

