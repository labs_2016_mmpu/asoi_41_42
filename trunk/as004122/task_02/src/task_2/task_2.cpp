/*! \mainpage ������ "Hello world!"
* ���������, ��������� �� ������� ������� "Hello world!"
* \file HelloWorld.cpp
* \brief ����, ������� ����� �������� �� ������� ������� "Hello world!"
*/
#include "stdafx.h"
#include <iostream>
using namespace std;

int main() //! ������� ������� ���������. ����� �������������� ����� �������.
{
	/*!
	����� ������� �� �����:
	\code
	cout << "Hello World" << endl;
	\endcode
	*/
	cout << "Hello World" << endl;
	/*!
	������ ��������� �� �����, ����� ���������� ���������� �������:
	\code
	system("pause");
	\endcode
	*/
	system("pause");
	/*!
	���� ��������� ��������� �������, ������� main ���������� ����:
	\code
	return 0;
	\endcode
	*/
    return 0;
}

