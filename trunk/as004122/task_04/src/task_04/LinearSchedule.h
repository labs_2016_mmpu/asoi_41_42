/*! \file LinearSchedule.h
* \brief Header file including description of the class LinearSchedule, prototypes of the function Temp(), and also description of the constructor and destructor.
*/

#ifndef _LINEARSCHEDULE_H
#define _LINEARSCHEDULE_H
#include <math.h>
#include <iostream>
#include "AbstractClass.h"

/*!Class LinearSchedule realizing linear model.
\brief Class inharited from abstract class.
*/

class LinearSchedule : public AbstractClass
{
public:
	LinearSchedule();
	~LinearSchedule();
	void LinearSchedule::Temp();
};
#endif 