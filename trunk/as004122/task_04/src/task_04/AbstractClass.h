/*! \file AbstractClass.h
* \brief Header file including prototypes of the functions PidRegular() and Temp(), description of the class AbstractClass, definition of global variables.
*/

#ifndef _ABSTRACTCLASS_H
#define _ABSTRACTCLASS_H

///@brief Abstract parent class. 

class AbstractClass
{
public:
	///@briefParameters of the regulator.
	double q0;
	double q1;
	double q2;
	double e1 = 0, e2 = 0, e3 = 0;
	double u = 0;
	double w = 50;
	double Temperature = 0;
	double T0 = 0.6;
	double k = 0.6;
	double Td = 0.05;
	double T = 0.6;
	AbstractClass();
	~AbstractClass();
	void AbstractClass::PidRegular();
protected:
	///@briefVirtual function.
	virtual void Temp() = 0;
};

#endif
