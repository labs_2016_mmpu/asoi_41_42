/** \file
\brief Task_04 source code

\mainpage

\image html D:\3.png
\image html D:\1.png
\image html D:\2.png

\author Panasuk Tatyana
\date 22.11.2016
*/
#include "AbstractClass.h"
#include "LinearSchedule.h"
#include "NonLinearSchedule.h"		
#include <iostream>
using namespace std;
///@briefFunction realizes creating of objects of a class and operates with them.
int main()
{
	setlocale(0, "");
	LinearSchedule a;
	a.Temp();
	NonLinearSchedule b;
	b.Temp();
	system("pause");
	return 0;
}