/*! \file AbstractClass.cpp
* \brief Source code including description of the function PidRegular(), descriptions of variables q0, q1,q2, e3, e2, e1, u, and also constructor and destructor of the class AbstractClass. 
*/

#include "AbstractClass.h"

///@briefFunction modeling the PID-regulator.


void AbstractClass::PidRegular()
{
	q0 = k*(1 + (Td / T0));
	q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = k * (Td / T0);
	e3 = e2;
	e2 = e1;
	e1 = w - Temperature;
	u += (q0 * e1 + q1 * e2 + q2 * e3);
}
///@brief Constructor of basic abstract class AbstractClass.
AbstractClass::AbstractClass() {}
///@brief Destructor of basic abstract class AbstractClass.
AbstractClass::~AbstractClass() {}