/*! \file LinearSchedule.cpp
* \brief Source code including definition of the function Temp() for the class LinearSchedule, and also constructor and destructor of the LinearSchedule.
*/


#include "LinearSchedule.h"
#include "AbstractClass.h"
///@briefFunction modeling PID-regulator of the linear model.

void LinearSchedule::Temp()
{
	std::cout << "Solution of the linear equation\n\n";
	std::cout.width(11);
	std::cout << "Temperature" << "  |  " << "�ontrol action\n\n";
	double  Temperature_1 = 7;
	for (int i = 0; i <= 25; i++)
	{
		PidRegular();

		Temperature_1 = 0.988*Temperature + 0.232*u;
		Temperature = Temperature_1;
		std::cout.width(11);
		std::cout << Temperature_1 << "  |  " << u << std::endl;
	}
}
///@briefConstructor of the class LinearSchedule.
LinearSchedule::LinearSchedule() {}
///@briefDestructor of the class LinearSchedule.
LinearSchedule::~LinearSchedule() {}

