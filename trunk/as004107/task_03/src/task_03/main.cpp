///@File
///@Author Voytenko Stanislav
///@14.11.2016
#include "Liner.h"
#include "NLiner.h"
#include <iostream>

using namespace::std;
///@param Input temperature 
const double ut = 7;

///@mainpage Graphs of Temperature 
///@image html Liner.png
///@image html NLiner.png
int main()
{
	Liner *ObjectLiner = new Liner;
	NLiner *ObjectNLiner = new NLiner;
	ObjectLiner->ShowResult();
	ObjectNLiner->ShowResult();
	system("pause");
	return 0;
}