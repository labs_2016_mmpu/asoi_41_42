///@file
///@author Kelbeda Sergey
///@23.10.2016

#include<iostream>
#include<conio.h>

///@brief Write "Hello world on console"

int main(){
     ///@brief Write "Hello world on console"
	std::cout << "Hello world"<<std::endl;

	///@brief Wait  when user press on any button
	system("pause");

	
	return 0;
}