#include "linear.h"

using namespace std;

void linear::show() {
	///@brief calculating the linear model
	cout << "Linear method" << endl << endl;
	for (int t = 1; t <= 10; t++) {
		Y[t + 1] = 0.988*Y[t] + 0.232*Ut;
		cout << "Y[" << t << "]\t=\t" << Y[t] << endl;
	}
	cout << endl;
}
