#include "linear.h"
#include "nonLinear.h"
#include <iostream>

///@file
///@10.12.2016
///@Author Mokej Dima
///@mainpage Model of Temperature 
///@image html Linear.jpg
///@image html NonLinear.jpg
using namespace std;

///@brief We creating objects and show them
int main(int argc, char* argv) {
	///@brief create object of linear
	linear Linear;
	///@brief create object of nonLinear
	nonLinear	NonLinear;
	Linear.show();
	NonLinear.show();
	system("pause");
	return 0;
}