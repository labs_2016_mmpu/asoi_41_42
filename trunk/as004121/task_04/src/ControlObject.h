#pragma once
#include <iostream>
#include <cmath> 
#include <iomanip> 
using namespace::std;

///@brief Abstract class
class ControlObject
{
protected:
	const int size = 20;
	double *Temp = new double[size];
public:
	///@param variables for the task
	double Ti = 0.4;
	double Td = 0.3;
	double K = 0.5;
	double T0 = 0.7;
	double e1 = 0;
	double e2 = 0;
	double e3 = 0;
	double q0 = 0;
	double q1 = 0;
	double q2 = 0;
	double u = 0;
	double w = 30;
	
	void show()
	{
		for (int i = 1; i<size; i++)
		{
			cout << setw(3) << i << ": " << Temp[i] << endl;
		}
	}
	virtual void modeling() = 0;
	void PidController(int i);
	ControlObject()
	{
		for (int i = 0; i<size; i++)
			Temp[i] = 0;
	}
};