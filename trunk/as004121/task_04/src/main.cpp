#include "Line.h"
#include "Nonline.h"
#include <iostream>
using namespace std;

///@file
///@10.12.2016
///@author Mokej Dima
///@mainpage Model of Temperature 
///@image html Line.bmp
///@image html Nonline.bmp
using namespace std;

int main()
{
	line q1;
	q1.modeling();
	cout << "Line" << endl;
	q1.show();
	cout << endl << endl << endl << endl;
	NonLine Q1;
	cout << "Nonline" << endl;
	Q1.modeling();

	Q1.show();
	system("pause");
	return 0;
}