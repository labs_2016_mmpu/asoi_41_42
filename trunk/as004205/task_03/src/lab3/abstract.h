#pragma once

///@brief abstract class
class abstract
{
public:
	///@brief virtual method for calculation of parameters
	virtual void fill() = 0;
	///@brief virtual method for showing all parameters
	virtual void show() = 0;
protected:
	double y[15];
	///@param heat input
	const int u = 20;
};