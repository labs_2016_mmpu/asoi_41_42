#include "linear.h"
#include <iostream>
using namespace std;

void linear::fill()
{
	y[0] = 0;
	for (int t = 0; t < 14; t++)
	{
		y[t + 1] = 0.988*y[t] + 0.232*u;
	}
}

void linear::show()
{
	cout << "Linear:" << endl;
	cout << "y" << "\t" << "u" << "\t" << "t" << endl;
	for (int t = 0; t < 15; t++)
	{
		cout << t << "\t" << y[t] << "\t" << u << endl;
	}
}