﻿/**
@file
@author Artsiom Denisiuk
@date 11.11.2016
@mainpage Temperature models
@image html image.png
*/
#include <iostream>
#include <cstdlib>
#include "abstract.h"
#include "linear.h"
#include "nonlinear.h"
using namespace std;

///@brief entry point of the program
int main()
{
	linear lin;
	nonlinear nonlin;
	///@brief creating and displaying parametrs of linear model
	lin.show();
	cout << endl;
	///@brief creating and displaying parametrs of nonlinear model
	nonlin.show();
	system("pause");
	return 0;
};