// Hello_World.cpp : Defines the entry point for the console application.
//
///@file
///@author Alexandr Martynov
///@21.10.2016 
#include "stdafx.h"
#include <iostream>
using namespace std;

///@brief function display into screen "Hello World" 
int main()
{
	cout << "Hello World!" << endl;
	return 0;
}

