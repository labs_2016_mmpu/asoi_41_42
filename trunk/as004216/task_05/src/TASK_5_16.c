#include"I7188.h"
/** @file 
@mainpage
@author Alexandr Martynov
@date 28.11.2016

@brief This program show into controller's display "hello world" statement

@brief This images show how program work

@image html task_5_1.jpg
@image html task_5_2.jpg
@image html task_5_3.jpg

*/

///@brief This program show into controller's display "hello world" statement
void main()
{

Show5DigitLedSeg(1,55); 
Show5DigitLedSeg(2,79); 
Show5DigitLedSeg(3,14); 
Show5DigitLedSeg(4,14); 
Show5DigitLedSeg(5,126);
DelayMs(1000);
Show5DigitLedSeg(1,79); 
Show5DigitLedSeg(2,14); 
Show5DigitLedSeg(3,14); 
Show5DigitLedSeg(4,126);
Show5DigitLedSeg(5,0);  
DelayMs(1000);
Show5DigitLedSeg(1,14); 
Show5DigitLedSeg(2,14); 
Show5DigitLedSeg(3,126);
Show5DigitLedSeg(4,0);  
Show5DigitLedSeg(5,62); 
DelayMs(1000);
Show5DigitLedSeg(1,14); 
Show5DigitLedSeg(2,126);
Show5DigitLedSeg(3,0); 
Show5DigitLedSeg(4,62); 
Show5DigitLedSeg(5,126);
DelayMs(1000);
Show5DigitLedSeg(1,126);
Show5DigitLedSeg(2,0);  
Show5DigitLedSeg(3,62); 
Show5DigitLedSeg(4,126);
Show5DigitLedSeg(5,70); 
DelayMs(1000);
Show5DigitLedSeg(1,0);  
Show5DigitLedSeg(2,62); 
Show5DigitLedSeg(3,126);
Show5DigitLedSeg(4,70); 
Show5DigitLedSeg(5,14); 
DelayMs(1000);
Show5DigitLedSeg(1,62);
Show5DigitLedSeg(2,126);
Show5DigitLedSeg(3,70); 
Show5DigitLedSeg(4,14); 
Show5DigitLedSeg(5,61); 
DelayMs(1000);

}