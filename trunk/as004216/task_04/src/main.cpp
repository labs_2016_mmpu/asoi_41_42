#include "stdafx.h"
/** @file

@mainpage
@author Martynov Alexandr

@date 18.11.2016


@image html line_pid.png

@image html nonLine_pid.png

@brief nonline model regulated better than line model

*/
#include"MMYP4.h"
#include <iostream>
using namespace std;

///@brief in function main applied the metods of classes line_temperature and dif_temperature with PID-regulation
int main() {
	double u = 30, w = 70, y_val = 50;
	double t0 = 0.1, td = 0.02, t = 0.4, k = 0.4;
	line_temperature t1(y_val);
	pid pid1(t0, td, t, k), pid2(t0, td, t, k);
	for (int i = 0; i<10; i++) {
		cout << i + 1 << "	";
		u = pid1.Calculate(y_val, w);
		y_val = t1.calculate(u);
	}
	cout << endl;
	y_val = 50;
	u = 30;
	dif_temperature t2(y_val);
	for (int i = 0; i<10; i++) {
		cout << i + 1 << "	";
		u = pid2.Calculate(y_val, w);
		y_val = t2.calculate(u);
	}
	system("pause");
	return 0;
}
