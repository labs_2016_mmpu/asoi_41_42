#pragma once
#include "stdafx.h"

///@brief class temperature is a parent class
class temperature {
protected:
	double y;
public:
	temperature(double y1);
	~temperature();
	virtual double calculate(double u) = 0;
};
///@brief class line_temperature calculate the value of line function
class line_temperature :public temperature {
public:
	line_temperature(double y1) :temperature(y1) {};
	///@brief function calculate count and display the value of line function
	double calculate(double u);
};
///@brief class dif_temperature calculate the value of nonline function
class dif_temperature :public temperature {
	double y_old;
public:
	dif_temperature(double y1) :temperature(y1) {
		y_old = y1;
	}
	///@brief function calculate count and display the value of nonline function
	double calculate(double u);
};
///@brief in class pid released the PID-regulator
class pid
{
private:
	double T0, Td, T, K, q0, q1, q2, e1, e2, e3, u;

public:

	///@brief algorithm of the PID-regulation

	double Calculate(double y, double w);
	pid(double T0, double Td , double T , double K);
	~pid() { }
};
