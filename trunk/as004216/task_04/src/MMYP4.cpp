// MMYP4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include"MMYP4.h"
using namespace std;

temperature::temperature(double y1) {
	y = y1;
}
temperature::~temperature() {
	y = NULL;
}
///@brief function calculate count and display the value of line function	
double line_temperature::calculate(double u) {
	y = 0.988*y + 0.232*u;
	cout << y << endl;
	return y;
}
///@brief function calculate count and display the value of nonline function
double dif_temperature::calculate(double u) {
	double tmp = y;
	y = 0.9*y - 0.001*y_old + u + sin(u);
	y_old = tmp;
	cout << y << endl;
	return y;
}

///@brief algorithm of the PID-regulation
double pid::Calculate(double y, double w)

{
	e3 = e2;
	e2 = e1;
	e1 = w - y;
	u += (q0 * e1 + q1 * e2 + q2 * e3);
	return u;
}



pid::pid(double t0, double td, double t, double k)

{
	T0 = t0;
	Td = td;
	T = t;
	K = k;
	u = 0;
	q0 = K* (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);
}
