#pragma once
#include "stdafx.h"
#include <iostream>
#include <cmath>
using namespace std;
///@brief class temperature is a parent class
class temperature {
protected:
	double u;
	double y;
public:
	temperature(double u1, double y1);
	~temperature();
	virtual void calculate() = 0;
};
///@brief class line_temperature calculate the value of line function
///@image html line.bmp
class line_temperature :public temperature {
public:
	line_temperature(double u1, double y1) :temperature(u1, y1) {};
///@brief function calculate count and display the value of line function
	void calculate();
};
///@brief class dif_temperature calculate the value of nonline function
///@image html nonline.bmp
class dif_temperature :public temperature {
	double y_old;
public:
	dif_temperature(double u1, double y1) :temperature(u1, y1) {
		y_old = y1;
	}
///@brief function calculate count and display the value of nonline function
	void calculate();
};
