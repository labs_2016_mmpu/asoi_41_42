///@mainpage  A console application that displays the "Hello World!"
///@file
///@author Khantsevich Oleg
///@date 23.10.2016
#include<iostream>
///@brief We define an entry point in the program
void main()
{
	///@brief Display message "Hello,world"
	std::cout << "Hello,World!";
	///@brief We set the display console
	system("pause");
}