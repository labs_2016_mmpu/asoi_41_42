
/** \file
\brief Example of functioning of hierarchy line
*/
#pragma once
#include "stdafx.h"
#include "ControlObject.h"

/// The linear model of a control object is described. Is a successor of a class ControlObject.
class line : public ControlObject
{
public:
	/// obtaining result of simulation of the linear system
	double modeling(double Y, double U);

	line() : ControlObject() {};

	~line() {}

};
