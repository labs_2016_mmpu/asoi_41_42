
/** \file
\brief  Contains class definition pid
*/
#pragma once
#include "stdafx.h"
///Simulation of the PID-regulator
class pid
{
private:
	double T0, Td, T, K, q0, q1, q2;
	double e1 = 0, e2 = 0, e3 = 0, U = 0;
public:
	///This calc_q function calculates values q
	void calc_q();

	///Algorithm of operation of the PID-regulator
	double Calc(double Y, double W);
		
	pid(double T0 = 0.1, double Td = 0.02, double T = 0.4, double K = 0.4);
		
	~pid() { }




};

