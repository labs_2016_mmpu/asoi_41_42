/** \file
\brief Contains the source code of the program for lab 4.
\author Shelikhov Vladislav Eduardovich
\date 17.11.2016

\mainpage
\image html line.png
\image html nonLine.png
\brief Results showed that the non-linear model is regulated quicker

*/

#include "stdafx.h"
#include "pid.h"
#include "ControlObject.h"
#include "line.h"
#include "nonline.h"
#include <iostream>
#include <iomanip>

using namespace std;



///Example of functioning of hierarchy
int main()
{
	line lineObj;

	NonLine nonlineObj;

	pid PID;
	PID.calc_q();

	double Y = 30;
	double U = 30;

	double W = 50;


	for (int i = 0; i != 100; i++)
	{
		U = PID.Calc(Y, W);
		Y = lineObj.modeling(Y, U);
		cout << i << "  " << Y << endl;
	}
	cout << "--------------------------------------------" << endl;


	Y = 20;
	U = 30;
	for (int i = 0; i != 20; i++)
	{
		U = PID.Calc(Y, W);
		Y = nonlineObj.modeling(Y, U);
		cout << i << "  " << Y << endl;
	}
	system("pause");
	return 0;
}
