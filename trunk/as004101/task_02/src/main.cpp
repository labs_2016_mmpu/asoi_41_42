///@file
///@author Vladislav Balashenko
///@19.01.2017

#include <iostream>
using namespace std;

int main(){
  ///@brief Print "hello world"
	cout << "Hello world" << endl;
	return 0;
}