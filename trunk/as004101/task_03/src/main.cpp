///@file
///@author Vladislav Balashenko
///@19.01.2017

#include "linear.h"
#include "nonlinear.h"
#include <iostream>

using namespace std;

///@param input temperature
double u = 200;

///@mainpage Model of Temperature 
///@image html /home/hacked/mmpiu/asoi_41_42/trunk/as004101/task_03/src/line.jpg
///@image html /home/hacked/mmpiu/asoi_41_42/trunk/as004101/task_03/src/NonLiner.jpg
///@brief We create linear an nonlinear objects and run their show methods
int main() {
	linear lin;
	nonlinear non_lin;
	lin.show();
	cout << endl << endl;
	non_lin.show();
	return 0;
}