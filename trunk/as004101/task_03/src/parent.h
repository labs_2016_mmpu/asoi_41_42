///@file
#pragma once
#include <iostream>

///@brief parent class
class parent {
public:
	///@show virtual method
	virtual void show() = 0;
protected:
	double y[10];
};