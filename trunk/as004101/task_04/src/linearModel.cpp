///@file
#include "linearModel.h"
#include <iostream>
#include <fstream>

using namespace std;

extern double T0, Td, T, K, q0, q1, q2, e1, e2, e3, u, w, y;
///@brief Method for the linear model
void linearModel::show() {
	cout << "\tLinear model" << endl << endl;
	cout.width(10);
	cout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
	double Y = 0;
	cout.precision(3);
	ofstream fout("/home/hacked/linear.txt", ios::out);
	fout << "\tLinear model" << endl << endl;
	fout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
	for (int t = 0; t < 150; t++) {
		pid();
		Y = 0.988*y + 0.232*u;
		y = Y;
		cout.width(10);
		cout << Y << "\t" << u << "\t\t" << t << endl;
		fout << Y << "\t" << u << "\t\t" << t << endl;
	}
	fout.close();
}