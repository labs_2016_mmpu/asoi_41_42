///@file
///@author Vladislav Balashenko
///@19.01.2017

#include "linearModel.h"
#include "nonlinearModel.h"
#include <iostream>
#include <fstream>

using namespace std;

///@param parameters for PID-controller
double T0 = 0.4, Td = 0.01, T = 0.85, K = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0, w = 20, y = 0;

///@mainpage Model of Temperature 
///@image html /home/hacked/mmpiu/asoi_41_42/trunk/as004101/task_04/src/image.png
///@brief We create objects and display them on the screen
int main() {
	linearModel lin;
	nonlinearModel nonlin;
	lin.show();
	cout << endl << endl;
	nonlin.show();
	return 0;
};