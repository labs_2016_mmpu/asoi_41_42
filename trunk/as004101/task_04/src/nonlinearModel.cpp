///@file
#include "nonlinearModel.h"
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

extern double T0, Td, T, K, q0, q1, q2, e1, e2, e3, u, w, y;

///@brief Method for the nonlinear model
void nonlinearModel::show() {
	cout << "\tNonlinear model" << endl << endl;
	cout.width(10);
	cout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
	double Y = 0;
	double Y1 = 0;
	cout.precision(5);
	ofstream fout("/home/hacked/nonlinear.txt", ios::out);
	fout << "\tNonlinear model" << endl << endl;
	fout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
	for (int t = 1; t < 28; t++) {
		pid();
		Y = 0.9*Y1 - 0.001*y * y + u + sin(u);
		Y1 = Y;
		y = Y;
		cout.width(10);
		cout << Y1 << "\t" << u << " \t\t" << t << endl;
		fout << Y1 << "\t" << u << "\t\t" << t << endl;
	}
	fout.close();
}