///@file
#pragma once
#include "change.h"

///@class linearModel 
///@class  linearModel derived from abstract
class linearModel: public abstract {
public:
	///@brief Method for the linear model
	void show();
};