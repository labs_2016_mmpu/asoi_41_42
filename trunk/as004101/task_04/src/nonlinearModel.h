///@file
#pragma once
#include "change.h"

///@class nonlinearModel 
///@class  nonlinearModel derived from abstract
class nonlinearModel : public abstract
{
public:
	///@brief Method for the nonlinear model
	void show();
};