//
// Created by vlad on 08/12/16.
//

#ifndef LAB4MMPIU_NONLINEARMODEL_H
#define LAB4MMPIU_NONLINEARMODEL_H

#include "ObjectModel.h"

class NonLinearModel : public ObjectModel{

public:
    void getObjectModeling();
};


#endif //LAB4MMPIU_NONLINEARMODEL_H
