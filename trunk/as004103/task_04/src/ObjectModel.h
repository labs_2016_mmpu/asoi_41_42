//
// Created by vlad on 08/12/16.
//

#ifndef LAB4MMPIU_OBJECTMODEL_H
#define LAB4MMPIU_OBJECTMODEL_H

class ObjectModel {

protected:
    const int size = 20;

public:
    double *temp = new double[size];
    double Ti = 0.4;
    double Td = 0.3;
    double K = 0.5;
    double T0 = 0.7;
    double e1 = 0;
    double e2 = 0;
    double e3 = 0;
    double q0 = 0;
    double q1 = 0;
    double q2 = 0;
    double u = 0;
    double w = 30;

    virtual void  getObjectModeling() = 0;

    void pidController(int i);

    ObjectModel();
};


#endif //LAB4MMPIU_OBJECTMODEL_H
