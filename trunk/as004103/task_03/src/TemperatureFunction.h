//
// Created by vlad on 02/12/16.
//

#ifndef UNTITLED1_TEMPERATUREFUNCTION_H
#define UNTITLED1_TEMPERATUREFUNCTION_H

//abstract class
class TemperatureFunction {
public:
    //method, which returned temperature function
    virtual long double getFunction() ;
    TemperatureFunction(long double heat, long double outletTemperature);


protected:
    long double heat;
    long double outletTemperature;
};



#endif //UNTITLED1_TEMPERATUREFUNCTION_H
