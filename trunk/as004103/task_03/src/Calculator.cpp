//
// Created by vlad on 02/12/16.
//

#include <iostream>
#include "Calculator.h"

void Calculator::calculate(TemperatureFunction *temperatureFunction) {
    for (int i = 0; i < 20; ++i) {
        long double a = temperatureFunction->getFunction();
        std::cout << i << " " << a << std::endl;
    }
}
