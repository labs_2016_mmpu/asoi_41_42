//
// Created by vlad on 02/12/16.
//

#include "NonLinearTemperatureFunction.h"
#include <math.h>

NonLinearTemperatureFunction::NonLinearTemperatureFunction(long double heat, long double outletTemperature) : TemperatureFunction(
        heat, outletTemperature) {}

long double NonLinearTemperatureFunction::getFunction() {
    long double nextOutletTemperature = 0.9 * outletTemperature - 0.001 * pow(previousOutletTemperature, 2) + heat + sin(heat);
    previousOutletTemperature =  outletTemperature;
    outletTemperature = nextOutletTemperature;
    return nextOutletTemperature;
}