//
// Created by vlad on 02/12/16.
//

#include "LinearTemperatureFunction.h"

long double LinearTemperatureFunction::getFunction() {
        outletTemperature = 0.988*outletTemperature + 0.232*heat;
        return outletTemperature;
}

LinearTemperatureFunction::LinearTemperatureFunction(long double heat, long double outletTemperature) : TemperatureFunction(heat,
                                                                                                                  outletTemperature) {
}
