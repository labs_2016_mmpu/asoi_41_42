//
// Created by vlad on 02/12/16.
//

#ifndef UNTITLED1_CALCULATOR_H
#define UNTITLED1_CALCULATOR_H

#include "TemperatureFunction.h"

class Calculator {
public:
    //method, which calculate temperature function
    void calculate(TemperatureFunction *temperatureFunction);
};


#endif //UNTITLED1_CALCULATOR_H
