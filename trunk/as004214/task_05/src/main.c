/**
 * \file
 * \brief Программа для отображения надписи HELLO WORLD на контролере
 * \mainpage "HELLO WORLD"
 * \author Левончук Вячеслав
 * \date 12.12.2016
 */


#include "I7188.H"

unsigned char p_p = 0;
unsigned char H = 55;
unsigned char E = 79;
unsigned char L = 14;
unsigned char O = 126;
unsigned char W = 62;
unsigned char R = 70;
unsigned char D = 61;

void main()
{
    while(1)
    {
        Show5DigitLedSeg(1,p_p);
        Show5DigitLedSeg(2,p_p);
        Show5DigitLedSeg(3,p_p);
        Show5DigitLedSeg(4,p_p);
        Show5DigitLedSeg(5,p_p);
        DelayMs(1000);
        Show5DigitLedSeg(1,p_p);
        Show5DigitLedSeg(2,p_p);
        Show5DigitLedSeg(3,p_p);
        Show5DigitLedSeg(4,p_p);
        Show5DigitLedSeg(5,H);
        DelayMs(1000);
        Show5DigitLedSeg(1,p_p);
        Show5DigitLedSeg(2,p_p);
        Show5DigitLedSeg(3,p_p);
        Show5DigitLedSeg(4,H);
        Show5DigitLedSeg(5,E);
        DelayMs(1000);
        Show5DigitLedSeg(1,p_p);
        Show5DigitLedSeg(2,p_p);
        Show5DigitLedSeg(3,H);
        Show5DigitLedSeg(4,E);
        Show5DigitLedSeg(5,L);
        DelayMs(1000);
        Show5DigitLedSeg(1,p_p);
        Show5DigitLedSeg(2,H);
        Show5DigitLedSeg(3,E);
        Show5DigitLedSeg(4,L);
        Show5DigitLedSeg(5,L);
        DelayMs(1000);
        Show5DigitLedSeg(1,H);
        Show5DigitLedSeg(2,E);
        Show5DigitLedSeg(3,L);
        Show5DigitLedSeg(4,L);
        Show5DigitLedSeg(5,O);
        DelayMs(1000);
        Show5DigitLedSeg(1,E);
        Show5DigitLedSeg(2,L);
        Show5DigitLedSeg(3,L);
        Show5DigitLedSeg(4,O);
        Show5DigitLedSeg(5,p_p);
        DelayMs(1000);
        Show5DigitLedSeg(1,L);
        Show5DigitLedSeg(2,L);
        Show5DigitLedSeg(3,O);
        Show5DigitLedSeg(4,p_p);
        Show5DigitLedSeg(5,L);
        DelayMs(1000);
        Show5DigitLedSeg(1,L);
        Show5DigitLedSeg(2,O);
        Show5DigitLedSeg(3,p_p);
        Show5DigitLedSeg(4,L);
        Show5DigitLedSeg(5,W);
        DelayMs(1000);
        Show5DigitLedSeg(1,O);
        Show5DigitLedSeg(2,p_p);
        Show5DigitLedSeg(3,L);
        Show5DigitLedSeg(4,W);
        Show5DigitLedSeg(5,O);
        DelayMs(1000);
        Show5DigitLedSeg(1,p_p);
        Show5DigitLedSeg(2,L);
        Show5DigitLedSeg(3,W);
        Show5DigitLedSeg(4,O);
        Show5DigitLedSeg(5,R);
        DelayMs(1000);
        Show5DigitLedSeg(1,L);
        Show5DigitLedSeg(2,W);
        Show5DigitLedSeg(3,O);
        Show5DigitLedSeg(4,R);
        Show5DigitLedSeg(5,L);
        DelayMs(1000);
        Show5DigitLedSeg(1,W);
        Show5DigitLedSeg(2,O);
        Show5DigitLedSeg(3,R);
        Show5DigitLedSeg(4,L);
        Show5DigitLedSeg(5,D);
        DelayMs(1000);
        Show5DigitLedSeg(1,O);
        Show5DigitLedSeg(2,R);
        Show5DigitLedSeg(3,L);
        Show5DigitLedSeg(4,D);
        Show5DigitLedSeg(5,p_p);
        DelayMs(1000);
        Show5DigitLedSeg(1,R);
        Show5DigitLedSeg(2,L);
        Show5DigitLedSeg(3,D);
        Show5DigitLedSeg(4,p_p);
        Show5DigitLedSeg(5,p_p);
        DelayMs(1000);
        Show5DigitLedSeg(1,L);
        Show5DigitLedSeg(2,D);
        Show5DigitLedSeg(3,p_p);
        Show5DigitLedSeg(4,p_p);
        Show5DigitLedSeg(5,p_p);
        DelayMs(1000);
        Show5DigitLedSeg(1,D);
        Show5DigitLedSeg(2,p_p);
        Show5DigitLedSeg(3,p_p);
        Show5DigitLedSeg(4,p_p);
        Show5DigitLedSeg(5,p_p);
        DelayMs(1000);
    }
}
