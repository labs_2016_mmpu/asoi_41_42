/**
 \file model.h
\authors Левончук Вячеслав.
\date 17.11.2016
 \brief Файл содержит классы необходимые для реальзации модели управления.
*/

#ifndef MMPIU3_MODEL_H
#define MMPIU3_MODEL_H

class fstream;

/**
\brief Абстрактный класс

В данном классе присутствуют переменные вычисляемые в наших моделях управления.
А так же виртуальный метод display, который будет наследован.
Реализован ПИД-регулятор в методе pidContr.
*/
class ObjectModel{
protected:
    double T0 = 0.2, Td = 0.01, T = 0.75, K = 0.2, q0 = 0, q1 = 0, q2 = 0, e1 = 0, e2 = 0, e3 = 0, w = 15;
    ///\brief Массив для хранения Y[t].
    double Y[40];
    ///\brief Константное значение Ut.
    double Ut=20.5;
public:
    virtual void display()= 0;
    virtual void count()=0;
    void pidContr(int i);
};

class ofstream;

/**
\brief Класс для линейной модели.

Дочерний класс.
В нем реализованы методы для подсчета и вывода значений функций для линейной модели.
*/
class LinearObjectModel : public ObjectModel{
public:
    void display();
    void count();
    LinearObjectModel(){};
    ~LinearObjectModel(){};
};


/**
\brief Класс для нелинейной модели.

Дочерний класс.
В нем реализованы методы для подсчета и вывода значений функций для нелинейной модели.
*/

class NotLinearObjectModel : public ObjectModel{
public:
    void display();
    void count();
    NotLinearObjectModel(){};
    ~NotLinearObjectModel(){};
};


#endif //MMPIU3_MODEL_H
