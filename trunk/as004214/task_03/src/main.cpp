/**
 \file main.cpp
 \brief Программа "Task 3".
\authors Левончук Вячеслав.
\date 20.10.2016
\details Выполнение задания 3. Реализация объекта управления.
С использованием ООП, на языке С++.
График линейной модели.
\image html linGraf.png
График нелинейной модели.
\image html neLinGraf.png
*/


#include <iostream>
#include <stdlib.h>
#include "model.h"

using namespace std;


/**
Основная функция написанной программы.
В ней создаем объекты классов линейной и нелинейной модели.
А также производим вызов метода display для созданых объектов.
\return  Ноль, если успешно выполнено.
*/

int main() {
    LinearObjectModel linear;
    NotLinearObjectModel notLinear;
    linear.display();
    notLinear.display();
    system("pause");
    return 0;
}
