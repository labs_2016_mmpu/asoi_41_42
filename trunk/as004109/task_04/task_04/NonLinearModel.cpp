 
///Implementation of the NonLinearModel class.

 #include "NonLinearModel.h"
 #include<cmath>
 #include <iostream>
 #include <fstream>
 using namespace std;

 void NonLinear::show()
 {
	 ofstream fout("nonlinear.txt", ios::out);
	 cout << "Nonlinear:" << endl;
	 cout << "y" << "\t" << "u" << "\t" << "t" << endl;
	 for (int t = 0; t < 30; t++)
	 {

		 PidController();
		 y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
		 y1 = y2;
		 y = y1;

		 cout << t << "\t" << y2 << "\t" << u << endl;

		 fout << t << "\t" << y2 << "\t" << u << endl;
	 }
 }
 