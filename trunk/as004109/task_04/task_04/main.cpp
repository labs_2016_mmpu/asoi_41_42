///Igor Dubina
#include <iostream>
#include <cstdlib>
#include "temperature.h"
#include "LinearModel.h"
#include "NonLinearModel.h"
using namespace std;

///@mainpage Model of Temperature 
///@image html img.PNG

///main function is the entry point to program
 int main()
 {
	 Linear lin;
	 NonLinear nonlin;  
	 lin.show();
	 cout << endl;	  
	 nonlin.show();
 };