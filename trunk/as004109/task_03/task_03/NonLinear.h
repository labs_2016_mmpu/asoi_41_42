///describes NLinear
#ifndef NONLINEAR_H
#define NONLINEAR_H
#include"Model.h"

class NLinear : public Model
{
	double y;
	double yprev;
	double ynxt;
	double Calculate();
public:
	NLinear();
};

#endif