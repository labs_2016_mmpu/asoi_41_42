MiniOS7 history

[2004/10/11]
(For all modules)
1.Add the command "runexe" & "runexeb", for only looking for ".exe" or ".com" program.

[2004/09/10]
(For all modules)
1.Modify the command "run".
  Last function: get the lastest file then run it.
  change the function to: get the lastest ".exe/.com/.bat" file then run it.
  (some times only modify the configure file(for example : ".ini" file, then the command "run" can not work.)

2.Modify the command "run n".
  Some times user want pass the parameter "1" or "2" ... to the program,
  but use the command "run 2" will get the second file to run, not get the last file and pass "2" as parameter.
  So change the command to "run [#n] [para1 [para2 ...]]"
  this also will have problem when the program want pass the parameter start with '#', but I think it will
  be OK for most user. 

[2004/09/09]
(For all modules)
1.fixed bug for INT 21H AH=40H.
  (old verson will lost to send out the last one byte.)

[2004/08/18]
(for 8000 only)
1.Add support module 8114H.
2.Fix: can not find the 87K module plug in slot 0.

[2004/08/17]
Fix:(All Modules)
command "led5 time on|off" can work again.

[2004/06/16]
For all modules:
Fix: command "run [p1] [p2]..." when p1 is not the file index, it will not pass the the execution file.

[2004/06/04]
Uptodate status:
1.EEPROM only support 24LC16(2K bytes), if user want use 24LC1024(128K) ICPDAS will
  support special version OS.
2.Add show some messages when update OS.(Erase flash & write image to flash memory & wait system reset.)

...

[2003/03/13] 
Fix:After POWER ON reset, OS can not recognize 24LC16. 
Modify:(iView only)Set the default LcdMode to 4.(original set to 0(No back-light)) 

[2003/02/20] 
Fixed:Can not run the program in disk B. 

[2003/02/10] 
Fix:command "time" will set hour 23 to 0. 

[2003/01/28] 
modify:command "use eeprom":support 24LC1024. 
Fix: system setup data stored address in EEPROM(24LC16). 

[2003/01/27] 
(I8000)Fix:INT 16H can support S-MMI keys now. 

[2003/01/24] 
(I-8000)Fix the problem: in the function "USE COM0" can not change to slot 4-7. 
(all)Add support EEPROM 24LC1024. Command "diag" will show the EEPROM type. 
