/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Demo6.prj
   	    Demo6.c
	    i7188s.lib
4. Explain: (1). Writing initial data to all 2048(8*256) bytes EEPROM
            (2). Reading & showing the EEPROM data for check
            (3). The EEPROM can be erase/write in about 100,000 times
            (4). The EEPROM can be read in any number of time
            (5). The EEPROM can be used to store the 7188's module address & configuration

5. Hordware: 7188
It's compiled by Tony
------------------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\LIB\i7188.h"
/*--------------------------------------------------------------------------------------*/

void  main()
{ int i,j,t,k;
  char c;
  int ret;

  int type;
  int ver;

  type=Is7188();	/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();	/*if used in minios7*/
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }

  Init5DigitLed();		/* 5-digit LED initial */
  EnableEEP();			/* enable for write operation */

  Puts("\n\rWrite EEPROM : ");
  for (j=0; j<8; j++){		/* write initial value to all 1K bytes data */
     Print("\n\rwriting block=%x\n\r",j);
     Show5DigitLed(1,j);
     Show5DigitLed(2,17);
     for (i=0; i<256; i++) {
	  Show5DigitLed(5,i%10);
	  Show5DigitLed(4,(i/10)%10);
	  Show5DigitLed(3,i/100);
	  ret=WriteEEP(j,i,(i+j)&0xff);
	  Print("\raddr=%2X,data=%2X ret=%d",i,(i+j)&0xff,ret);
     }
  }
  Print("\n\r");
  ProtectEEP();			/* protect data in EEPROM  */

  for (k=0;k<256;k++) {
    i++; i=i&0x01;
    if (i==0) LedOn();	else LedOff();
    Print("\n\rRead EEPROM : address=%d(%X) data in block 0-7=",k,k);
    for (j=0; j<8; j++) {
	t=ReadEEP(j,k);        /* read back EEPROM for check */
	Print("%02X ",t);
    }
/*    DelayMs(300);  */		/* delay about 0.3 sec */

    while (Kbhit()) {
       c=Getch();
       if ((c=='q')|| (c=='Q')) goto End;
       Print("(%c)",c);
    }
  }
End:
  ;
}

