/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: test7050.prj
   	    test7050.c
	    i7188s.lib
4. Explain: (1) The led of 7050D will take turn to turn on then turn off .
            (2) To press 'q' or 'Q' to quit this program.
            **Their situation is as fallow:

[PC]<---------->[7188E]<------------------->[7520]<--------->[7050]
	RS232  (COM4) (COM1)  RS232/RS485  (RS232)   (RS485)  (RS485)

5. Hordware: 7188E with 7050
It's compiled by Tony
---------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include "..\lib\i7188.h"
/*--------------------------------------------------------------------------------*/

#define TimeOut 10000L

main()
{
   int i,y;
   unsigned char cmd[80], result[80];
    int type;
  int ver;
  type=Is7188();		/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();
     Print("Hello 7188! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
  InstallCom1(115200L,8,0,1);
  Init5DigitLed();
  Show5DigitLed(1,1);
  Show5DigitLed(2,2);
  Show5DigitLed(3,3);
  Show5DigitLed(4,4);
  Show5DigitLed(5,5);
  i=y=0;
  for (;;)
    {
    Show5DigitLed(1,i++);
    i=i&0x17;
    y=y&0x03;
    DelayMs(1000);
    if (y==1) strcpy(cmd,"#010001");
     else if (y==2) strcpy(cmd,"#010002");
     else if (y==3) strcpy(cmd,"#010004");
     else if (y==0) strcpy(cmd,"#010008");
    SendCmdTo7000(1,cmd,0);
    y++;
    if (Kbhit())
      {
       Getch();
       return;
      }
    }
}


