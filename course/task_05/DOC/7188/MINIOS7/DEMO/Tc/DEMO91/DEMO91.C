/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Demo91.prj
   	    Demo91.c
	    i7188s.lib
4. Explain: 1.Demonstration for use Timer functions.
   	    2.To use CountDownTimer channel 0 for LED ON/OFF.
5. Hordware: 7188
It's compiled by Tony
---------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<math.h>
#include<string.h>
#include"..\LIB\i7188.h"
/*--------------------------------------------------------------*/

void main(void)
{ int LedMode=0;
  unsigned long value;
  int quit=0;
  unsigned Ver;

  if(!Is7188()){	/*detect the current operation system*/
     Print("\ndemo91.exe must run on I-7188x with MiniOS7");
     return;
  }
  Ver=GetLibVersion();
  Print("\n\rLib version=%d.%02d",Ver>>8,Ver&0xff);
  Print("\n\rTest CountDownTimer...");
  Print("\n\rNow LED must flash once every second");
  Print("\n\rPress 'q' to quit\n\r");
  TimerOpen();
  LedOff();
  CountDownTimerStart(0,500);		/*to use the CountDownTimerStart function*/
  while(!quit){
     if(Kbhit()){
	switch(Getch()){
	  case 'q':
	       quit=1;
	       break;
	}
     }
     CountDownTimerReadValue(0,&value);
     if(value==0){
	CountDownTimerStart(0,500);
	if(LedMode){
	   LedMode=0;
	   LedOff();
	}
	else {
	   LedMode=1;
	   LedOn();
	}
     }
  }
  TimerClose();
  LedOn();
}

