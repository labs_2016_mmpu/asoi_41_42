*******************************************************
* gotc.bat    : batch file for use TC 2.0
* gomsc.bat   : batch file for use MSC 5.0/6.0 
* demo97.prj  : project file for TC++3.0 or BC++ 3.1
*******************************************************
  DEMO97
   Demonstration for use Timer functions.
   use DelayMs for LED ON/OFF
   function used:
     TimerOpen --> begin to use I-7188 timer
     TimerClose --> stop to use timer
     DelayMs  -->  delay time interval. unit is ms
   press 'q' to quit program.