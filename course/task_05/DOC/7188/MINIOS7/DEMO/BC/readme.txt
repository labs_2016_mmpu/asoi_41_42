7521 :    Demo program for 7521 as an Addressable RS-485 to RS-232 converter.
DateTime : Get the system Date and Time
eeprom:	   Multi Read and write EEPROM test
file:  	   Open File
flash:     All of the flash function
led :      LED function test
reset:	   7188 boot time test
watchdog:   demo program for I-7188 to test if I-7188 is reset by watchdog timer.
-------------------------------------------------------------------------------
demo6:	   read-EEPROM & write-EEPROM test
-------------------------------------------------------------------------------
demo32:    Connect to Touch7(Touch 200) +7060+7013+7017+7021(DemoBox2)
demo32-1:  Demo program run on I-7188 with MiniOS7. For Trainning BOX II
	   Using new driver to connect to Touch-200
-------------------------------------------------------------------------------
demo90: Demonstration for use Timer
   	function used:TimerOpen,TimerClose,TimerReadValue,TimerResetValue.
demo91: Demonstration for use Timer functions.
   	use CountDownTimer channel 0 for LED ON/OFF
demo92: Demonstration for use Timer functions.
	use StopWatch channel 0 for LED ON/OFF
   	functions: TimerOpen,TimerClose,StopWatchStart,StopWatchReadValue.
demo96: Demonstration for use Timer functions.
   	use User timer function to do the same thing as demo94.c
   	functions: TimerOpen,TimerClose,InstallUserTimer;
demo97: Demonstration for use Timer functions.
   	use DelayMs for LED ON/OFF
demo98: use I-7188 timer function to send/receive data to/from 7000's modules.
   	and set timeout in time unit ms. COM2: 9600,N,8,1 RS-485
-------------------------------------------------------------------------------
