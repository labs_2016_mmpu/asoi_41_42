/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Scanf.prj
   	    Scanf.c
	    i7188s.lib
4. Explain: (1) Show how to write a function for input data.
   	    (2) To get a string.
            (3) To use C function: sscanf,or just use Scanf().
	    ** MSC 6.0 cannot use Scanf(), because don't support function vsscanf()	
5. Hordware: 7188
It's compiled by Tony
------------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\LIB\i7188.h"
/*--------------------------------------------------------------------------------*/

main()
{ int i;
  long l;
  int type;
  int ver;
  type=Is7188();		/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();
     Print("Hello 7188! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else
     {
     Print("Hello PC!, this program is not run under 7188.");
     return;
     }
  Print("\r\nInput an integer:");
  Scanf("%d",&i);
  Print("Value=%d\n\r",i);

  Print("Input an long integer:");
  Scanf("%ld",&l);
  Print("Value=%ld\n\r",l);

  Print("Input an Hex value:");
  Scanf("%x",&i);
  Print("Value=%04X\n\r",i);

}