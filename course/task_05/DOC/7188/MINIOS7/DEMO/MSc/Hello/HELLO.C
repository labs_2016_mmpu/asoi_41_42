
/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Hello.prj
   	    Hello.c
	    i7188s.lib
4. Explain: (1). To detect the current operation system.
            (2). If you used this program in minios7,it will show 7188's version 
                 and "*** Hello I-7188! ***."
            (3). If you used this program in pure dos or dos-box in win98, 
                 it will show "Hello PC!, this program is not run under I-7188."
5. Hordware: 7188

It's compiled by Tony
------------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*----------------------------------------------------------------------------------*/

void main(void)
{ 
  int type;
  int ver;
  type=Is7188();	/*detect the current operation system*/
   if(type)		/*if it's used in minios7*/
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if it's used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
  Puts("*** Hello I-7188! ***\n\r");
}
